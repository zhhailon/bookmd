#!/usr/bin/env python3
#### Copyright (c) 2013-2021, Elliott Slaughter <elliottslaughter@gmail.com>
####
#### Permission is hereby granted, free of charge, to any person
#### obtaining a copy of this software and associated documentation
#### files (the "Software"), to deal in the Software without
#### restriction, including without limitation the rights to use, copy,
#### modify, merge, publish, distribute, sublicense, and/or sell copies
#### of the Software, and to permit persons to whom the Software is
#### furnished to do so, subject to the following conditions:
####
#### The above copyright notice and this permission notice shall be
#### included in all copies or substantial portions of the Software.
####
#### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#### NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
#### HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
#### WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#### OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#### DEALINGS IN THE SOFTWARE.
####

from __future__ import print_function, unicode_literals

import argparse, collections, datetime, json, multiprocessing, os, platform, re, shutil, subprocess, sys, tempfile, traceback

# For some reason I already had glob defined below, so I had to rename this...
# I don't know why I didn't just use glob.glob in the first place
import glob as glob_

###
### Compatibility for Python 2.x/3.x
###

_version = sys.version_info.major

if _version == 2: # Python 2.x:
    # Use binary mode to avoid mangling newlines.
    import codecs
    def _open(filename, mode='r'):
        return codecs.open(filename, mode, encoding='utf-8')
elif _version == 3: # Python 3.x:
    # Use text mode with UTF-8 encoding and '\n' newlines:
    def _open(filename, mode='r'):
        return open(filename, mode=mode, encoding='utf-8', newline='\n')
else:
    raise Exception('Incompatible Python version')

if _version == 2: # Python 2.x:
    def bytes(s, encoding=None):
        return str(s)
elif _version == 3: # Python 3.x:
    pass
else:
    raise Exception('Incompatible Python version')

if _version == 2: # Python 2.x:
    def glob(path):
        def visit(result, dirname, filenames):
            for filename in filenames:
                result.append(os.path.join(dirname, filename))
        result = []
        os.path.walk(path, visit, result)
        return result
elif _version == 3: # Python 3.x:
    def glob(path):
        return [os.path.join(dirname, filename)
                for dirname, _, filenames in os.walk(path)
                for filename in filenames]
else:
    raise Exception('Incompatible Python version')

try: # Python 2.x:
    basestring
except NameError: # Python 3.x:
    basestring = str

try: # Python 2.x:
    raw_input
except NameError: # Python 3.x:
    raw_input = input

###
### Configuration
###

def load_config(filename):
    with _open(filename, mode='r') as f:
        return json.load(f, object_pairs_hook=collections.OrderedDict)

def write_config(config_values, filename):
    with _open(filename, mode='w') as f:
        json.dump(config_values, f, indent=4, separators=(',', ': '))

def query_config(config_defaults):
    print('Please verify that the following configuration is correct.')
    config = collections.OrderedDict()
    for query, default in config_defaults.items():
        while True:
            raw_value = raw_input('%s (%s)? ' % (query, json.dumps(default)))
            value = default
            if len(raw_value) > 0:
                try:
                    value = json.loads(
                        raw_value,
                        object_pairs_hook=collections.OrderedDict)
                except ValueError:
                    print('Please provide a valid JSON value.')
                    continue
            break
        config[query] = value
    return config

def init_config(config_filename, defaults_filename, force_query=False):
    config_defaults = load_config(defaults_filename)
    previous_config = {}
    if os.path.exists(config_filename):
        previous_config = load_config(config_filename)
    config_defaults.update(previous_config)
    if previous_config != config_defaults:
        force_query = True
    if force_query:
        config = query_config(config_defaults)
        write_config(config, config_filename)
    else:
        config = config_defaults
    return config

###
### VCS Integration
###

def is_git_repository(dirname):
    return 0 == subprocess.call(
        ['git', 'rev-parse'],
        stdout=subprocess.PIPE, stderr=subprocess.PIPE,
        cwd=dirname)

def git_node_short(dirname):
    return subprocess.check_output(
        ['git', 'rev-parse', '--short=12', 'HEAD'],
        cwd=dirname).decode(encoding='utf-8').strip()

def git_date(dirname):
    timestamp = subprocess.check_output(
        ['git', 'log', '-1', '--format=format:%at'],
        cwd=dirname).decode(encoding='utf-8')
    return datetime.datetime.fromtimestamp(float(timestamp)).strftime('%B %d, %Y')

def git_update(dirname):
    subprocess.check_call(
        ['git', 'pull', '--ff-only'],
        cwd=dirname)

def is_hg_repository(dirname):
    return 0 == subprocess.call(
        ['hg', 'status'],
        stdout=subprocess.PIPE, stderr=subprocess.PIPE,
        cwd=dirname)

def hg_node_short(dirname):
    return subprocess.check_output(
        ['hg', 'parent', '--template', '{node|short}'],
        cwd=dirname).decode(encoding='utf-8')

def hg_date(dirname):
    timestamp = subprocess.check_output(
        ['hg', 'parent', '--template', '{date}'],
        cwd=dirname).decode(encoding='utf-8')
    return datetime.datetime.fromtimestamp(float(timestamp)).strftime('%B %d, %Y')

def hg_update(dirname):
    subprocess.check_call(
        ['hg', 'pull', '-u'],
        cwd=dirname)

def which_vcs(dirname):
    if is_git_repository(dirname):
        return 'git'
    if is_hg_repository(dirname):
        return 'hg'
    return None

def vcs_node_short(vcs, dirname):
    if vcs == 'git':
        return git_node_short(dirname)
    if vcs == 'hg':
        return hg_node_short(dirname)
    assert Exception('Invalid VCS "%s"' % vcs)

def vcs_date(vcs, dirname):
    if vcs == 'git':
        return git_date(dirname)
    if vcs == 'hg':
        return hg_date(dirname)
    assert Exception('Invalid VCS "%s"' % vcs)

def vcs_update(vcs, dirname):
    if vcs == 'git':
        return git_update(dirname)
    if vcs == 'hg':
        return hg_update(dirname)
    assert Exception('Invalid VCS "%s"' % vcs)

###
### Context
###

# Client and server contexts are kept separate so we don't
# accidentally leak multiprocessing objects to client contexts.
class ClientContext:
    def __init__(self, project_dir, root_dir, config_path, skip_config, stack, stack_exe, pandoc_exe):
        self.project_dir = project_dir
        self.root_dir = root_dir
        stack_yaml = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'stack.yaml')
        self.pandoc = [stack_exe, '--stack-yaml', stack_yaml, 'exec', '--', 'pandoc'] if stack else [pandoc_exe]
        self.ghc_make = [stack_exe, '--stack-yaml', stack_yaml, 'ghc'] if stack else ['ghc', '--make']

        if not skip_config:
            self.config = init_config(
                config_path,
                os.path.join(self.root_dir, 'defaults.json'))
            self.input_dir = os.path.join(
                self.project_dir, self.config['Source Directory'])
            self.output_dir = os.path.join(
                self.project_dir, self.config['Output Directory'])

        self.project_vcs = which_vcs(self.project_dir)
        self.root_vcs = which_vcs(self.root_dir)

class ServerContext:
    def __init__(self, project_dir, root_dir, config_path, skip_config,
                 part, full, stack, stack_exe, pandoc_exe, thread_count):
        self.client_cx = ClientContext(project_dir, root_dir, config_path,
                                       skip_config, stack, stack_exe, pandoc_exe)
        self.project_dir = self.client_cx.project_dir
        self.root_dir = self.client_cx.root_dir
        self.project_vcs = self.client_cx.project_vcs
        self.root_vcs = self.client_cx.root_vcs

        if not skip_config:
            self.config = self.client_cx.config
            self.input_dir = self.client_cx.input_dir
            self.output_dir = self.client_cx.output_dir
            self.part = part
            self.full = full
            self.pandoc = self.client_cx.pandoc

            self.thread_pool = multiprocessing.Pool(thread_count)
            self.manager = multiprocessing.Manager()
            self.event_graph = {}
            self.output_filenames = {}

###
### Utilities
###

def find_input_filenames(input_dir, input_ext):
    return [
        os.path.relpath(filename, input_dir)
        for filename in glob(input_dir)
        if os.path.splitext(filename)[1] == input_ext and
        not os.path.basename(filename).startswith('.') and
        not os.path.basename(filename).startswith('_')]

def choose_output_filenames(input_filenames, output_ext, output_filenames):
    used = set(output_filenames.values())
    result = []
    for filename in input_filenames:
        # Search for candidates in order of legibility:
        #   1. The input filename (strip extension).
        #   2. The input filename (including extension).
        #   3. The input filename (strip extension) followed by numeric suffix.
        candidate = os.path.splitext(filename)[0]
        index = ''
        while candidate + str(index) + output_ext in used:
            if isinstance(index, basestring):
                if len(index) == 0:
                    index = os.path.splitext(filename)[1]
                else:
                    index = 1
            else:
                index = index + 1
        output_filename = candidate + str(index) + output_ext

        output_filenames[filename] = output_filename
        used.add(output_filename)
        result.append(output_filename)
    return result

def newer(filename1, filename2):
    if not os.path.exists(filename1):
        return False
    if not os.path.exists(filename2):
        return True
    return os.path.getmtime(filename1) > os.path.getmtime(filename2)

###
### Templates, Headers, Footers, etc.
###

if platform.system() == 'Windows':
    plugin_exe_ext = '.exe'
else:
    plugin_exe_ext = ''

plugin_ignore_ext = frozenset(['.hi', '.o'])

def find_plugins_helper(cx, candidates, basename, assume_hs_source_is_compiled):
    result = []
    for plugin_filename in sorted(candidates):
        plugin_basename = os.path.basename(plugin_filename)
        plugin_root, plugin_ext = os.path.splitext(plugin_filename)
        if (plugin_basename == basename or
            plugin_basename.startswith('%s.' % basename)):
            if plugin_ext == '.hs':
                if assume_hs_source_is_compiled:
                    assert((plugin_root + plugin_exe_ext) in result)
                else:
                    try:
                        result.remove(plugin_root + plugin_exe_ext)
                    except ValueError:
                        pass
                    result.append(plugin_filename)
            elif (plugin_ext not in plugin_ignore_ext and
                  os.path.isfile(plugin_filename) and
                  os.access(plugin_filename, os.X_OK)):
                result.append(plugin_filename)
    return result

def find_plugins(cx, basename, variadic, assume_hs_source_is_compiled):
    project_candidates = glob(
        os.path.join(cx.project_dir, cx.config['Plugin Directory']))
    result = find_plugins_helper(
        cx, project_candidates, basename, assume_hs_source_is_compiled)
    if len(result) > 0:
        if not variadic and len(result) > 1:
            raise Exception('Too many plugins for %s' % basename)
        return result

    default_candidates = glob(os.path.join(cx.root_dir, 'etc', 'plugins'))
    result = find_plugins_helper(
        cx, default_candidates, basename, assume_hs_source_is_compiled)
    if len(result) > 0:
        if not variadic and len(result) > 1:
            raise Exception('Too many plugins for %s' % basename)
        return result

    if not variadic:
        raise Exception('Unable to find plugin for %s' % basename)
    return result

def find_template(cx, basename):
    pattern = '%s*%s' % os.path.splitext(basename)
    project_pattern = os.path.join(
        cx.project_dir, cx.config['Template Directory'], pattern)
    project_filenames = glob_.glob(project_pattern)
    if project_filenames:
        return [os.path.realpath(p) for p in project_filenames]
    default_pattern = os.path.join(cx.root_dir, 'etc', 'templates', pattern)
    return [os.path.realpath(p) for p in glob_.glob(default_pattern)]

def get_variables(cx, format, full):
    formats = cx.config['Pandoc Variables']
    if format in formats:
        sizes = formats[format]
        size = 'full' if full else 'part'
        if size in sizes:
            return sizes[size]
    return {}

###
### Build Recipes
###

templates = {
    'header': {'flag': '--include-in-header'},
    'before-body': {'flag': '--include-before-body'},
    'after-body': {'flag': '--include-after-body'},
    'template': {'flag': '--template'},
    'reference': {
        'flag': '--reference-doc',
        'formats': set(['docx']),
    },
    'style': {
        'flag': '--css',
        'formats': set(['epub']),
    },
    'cover': {
        'flag': '--epub-cover-image',
        'formats': set(['epub']),
    },
    'font': {
        'flag': '--epub-embed-font',
        'formats': set(['epub']),
    },
    'metadata': {
        'flag': '--epub-metadata',
        'formats': set(['epub']),
    },
}

class Format:
    def __init__(self, name, file_ext, format_name=None, toc_depth_max=6,
                 extensions=[], flags=[], template_ext={None: ['']}):
        for v in template_ext.values():
            assert(isinstance(v, list))
        self.name = name
        self.file_ext = file_ext
        self.format_name = format_name if format_name is not None else name
        self.toc_depth_max = toc_depth_max
        self.extensions = extensions
        self.flags = flags
        self.template_ext = template_ext

    def active(self, cx):
        return (self.name == 'json' or
                self.file_ext[1:] in cx.config['Input File Types'] or
                self.file_ext[1:] in cx.config['Output File Types'])

input_formats = {
    'markdown': Format(name='markdown', file_ext='.md'),
    'org': Format(name='org', file_ext='.org'),
    'docx': Format(name='docx', file_ext='.docx'),
}

formats = {
    'json': Format(
        name='json',
        file_ext='.json',
        extensions=[],
    ),
    'latex': Format(
        name='latex',
        file_ext='.tex',
        # --smart is redundant except when --no-tex-ligatures is defined,
        # which is useful for LuaLaTeX for proper OpenType typography
        extensions=['+smart'],
        template_ext={None: ['.tex']},
    ),
    'docx': Format(
        name='docx',
        file_ext='.docx',
        extensions=[],
        template_ext={None: ['.docx']},
    ),
    'pdf': Format(
        name='latex',
        file_ext='.pdf',
        # --smart is redundant except when --no-tex-ligatures is defined,
        # which is useful for LuaLaTeX for proper OpenType typography
        extensions=['+smart'],
        template_ext={None: ['.tex']},
    ),
    'html': Format(
        name='html',
        file_ext='.html',
        format_name='html5',
        extensions=['+smart'],
        flags=['--mathml'],
        template_ext={None: ['.html']},
    ),
    'epub': Format(
        name='epub',
        file_ext='.epub',
        format_name='epub3',
        toc_depth_max=2,
        extensions=['+smart'],
        flags=['--mathml'],
        template_ext={None: ['.html'], 'style': ['.css'], 'cover': ['.jpg', '.png'], 'font': ['.otf', '.ttf']},
    ),
    'mobi': Format(name='mobi', file_ext='.mobi'),
}

intermediate_formats = dict(
    list(input_formats.items()) + [
        ('json', formats['json']),
        ('epub', formats['epub'])])

class Generator:
    def __init__(self, input_format_name, output_format_name, full):
        self.input_format = intermediate_formats[input_format_name]
        self.output_format = formats[output_format_name]
        self.full = full

        self.full_name = 'full' if full else 'part'
        self.template_ext = self.output_format.template_ext
        self.template_basenames = dict(
            (k, '%s.%s.%s%s' % (
                k, self.output_format.name, self.full_name, ext))
            for k, v in templates.items()
            for ext in (self.template_ext[k] if k in self.template_ext
                         else self.template_ext[None])
            if 'formats' not in v or self.output_format.name in v['formats'])
        self.input_plugin_basename = 'input_filter.%s.%s' % (
            self.input_format.name, self.full_name)
        self.plugin_basename = 'filter.%s.%s' % (
            self.output_format.name, self.full_name)

    def active(self, cx):
        return self.input_format.active(cx) and self.output_format.active(cx)

    def plugins(self, cx):
        return (find_plugins(cx, self.input_plugin_basename, True, False) +
                find_plugins(cx, self.plugin_basename, True, False))

    def compile(self, cx, input_filename, output_filename):
        if not newer(input_filename, output_filename):
            return False
        template_filenames = [
            (k, x)
            for k, v in self.template_basenames.items()
            for x in find_template(cx, v)
            if x is not None]
        input_plugins = find_plugins(cx, self.input_plugin_basename, True, True)
        plugins = find_plugins(cx, self.plugin_basename, True, True)

        toc_args = []
        if self.full and cx.config['TOC']:
            toc_args = [
                '--toc',
                '--toc-depth=%s' % (min(
                    self.output_format.toc_depth_max,
                    cx.config['TOC Depth'])),
            ]
        template_args = [
            x
            for k, v in template_filenames
            for x in (templates[k]['flag'], v)]
        plugin_args = (
            ['--filter=%s' % input_plugin
             for input_plugin in input_plugins] +
            ['--filter=%s' % plugin
             for plugin in plugins])

        variables = get_variables(cx, self.output_format.name, self.full)
        variable_args = []
        for k, vs in variables.items():
            if vs is None:
                variable_args.extend(('--variable', k))
            else:
                for v in vs:
                    variable_args.extend(('--variable', '%s=%s' % (k, v)))

        # Hack: These should be handled separately from variables.
        misc_args = []
        if 'pdf-engine' in variables and len(variables['pdf-engine']) > 0:
            misc_args.append('--pdf-engine=%s' % (variables['pdf-engine'][0]))
        elif 'latex-engine' in variables and len(variables['latex-engine']) > 0:
            misc_args.append('--pdf-engine=%s' % (variables['latex-engine'][0]))

        if ('number-sections' in variables and
            len(variables['number-sections']) > 0 and
            variables['number-sections'][0]):
            misc_args.append('--number-sections')

        if ('epub-chapter-level' in variables and
            len(variables['epub-chapter-level']) > 0):
            misc_args.append('--epub-chapter-level=%s' % (
                variables['epub-chapter-level'][0]))

        extensions = list(self.output_format.extensions)
        if ('tex-ligatures' in variables and
            len(variables['tex-ligatures']) > 0 and
            variables['tex-ligatures'][0] == 'no'):
            extensions.remove('+smart')
            extensions.append('-smart')

        in_format = self.input_format.format_name
        out_format = ''.join([self.output_format.format_name] + extensions)

        subprocess.check_call(
            cx.pandoc + [
             '--from=%s' % in_format,
             '--to=%s' % out_format,
             '--standalone'] +
            self.output_format.flags +
            toc_args +
            template_args +
            plugin_args +
            variable_args +
            misc_args +
            ['--output=%s' % output_filename,
             input_filename])
        return True

class MobiGenerator:
    def active(self, cx):
        if formats['mobi'].active(cx):
            assert formats['epub'].active(cx)
            return True
        return False

    def plugins(self, cx):
        return []

    def compile(self, cx, input_filename, output_filename):
        # KindleGen doesn't take an output parameter, so this will
        # only work if the input and output only differ on the file
        # extension.
        assert (os.path.splitext(input_filename)[0] ==
                os.path.splitext(output_filename)[0])

        if not newer(input_filename, output_filename):
            return False

        # KindleGen doesn't like directories with spaces, so copy to a
        # temporary directory to build.
        temp_dir = tempfile.mkdtemp()
        temp_input = os.path.join(temp_dir, os.path.basename(input_filename))
        temp_output = os.path.join(temp_dir, os.path.basename(output_filename))
        shutil.copyfile(input_filename, temp_input)

        retcode = subprocess.call(['kindlegen', temp_input],
                                  stdout=subprocess.PIPE)
        # KindleGen returns:
        #   * 0 on success without warnings
        #   * 1 on success with warnings
        #   * 2 on errors
        if retcode not in [0, 1]:
            raise Exception('KindleGen failed on %s' % input_filename)
        shutil.copyfile(temp_output, output_filename)
        shutil.rmtree(temp_dir)
        return True

_metadata = '''% {Title}
% {Author}
% {Date}
'''
class MetadataGenerator:
    def active(self, cx):
        return True

    def plugins(self, cx):
        return []

    def compile(self, cx, _, output_filename):
        commit_id = vcs_node_short(cx.project_vcs, cx.project_dir)
        commit_date = vcs_date(cx.project_vcs, cx.project_dir)

        proc = subprocess.Popen(
            cx.pandoc + [
             '--from=markdown',
             '--to=json',
             '--standalone'],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE)

        substitutions = {
            'Title': cx.config['Title'],
            'Author': cx.config['Author'],
            'Date': cx.config['Date'],
            'Commit ID': commit_id,
            'Commit Date': commit_date,
        }

        metadata_md = _metadata.format(**substitutions).format(**substitutions)
        metadata_json = proc.communicate(
            bytes(metadata_md, encoding='utf-8'))[0].decode(
                encoding='utf-8')
        assert proc.returncode == 0
        old_metadata = None
        if os.path.exists(output_filename):
            with _open(output_filename, 'r') as f:
                old_metadata = f.read()
        if metadata_json != old_metadata:
            with _open(output_filename, 'w') as f:
                f.write(metadata_json)
            return True
        return False

class ConcatenateGenerator:
    def active(self, cx):
        return True

    def plugins(self, cx):
        return find_plugins(cx, 'concat', False, False)

    def compile(self, cx, input_filenames, output_filename):
        plugins = find_plugins(cx, 'concat', False, True)
        assert(len(plugins) == 1)
        plugin = plugins[0]

        if not any(map(lambda x: newer(x, output_filename), input_filenames)):
            return False

        subprocess.check_call(
            [plugin, output_filename] + input_filenames)
        return True

class PluginGenerator:
    def active(self, cx):
        return True

    def plugins(self, cx):
        return []

    def compile(self, cx, input_filename, _):
        # If the plugin is not Haskell source, assume it is already
        # executable.
        if not os.path.splitext(input_filename)[1] == '.hs':
            return False

        # Otherwise, if the plugin is Haskell source, compile it.
        output_filename = os.path.splitext(input_filename)[0]
        if not newer(input_filename, output_filename):
            return False

        subprocess.check_call(cx.ghc_make + [input_filename])
        return True

generators = dict([
    ((name, 'json', False), Generator(name, 'json', False))
    for name in input_formats.keys()
] + list(({
    ('json', 'latex', False): Generator('json', 'latex', False),
    ('json', 'latex', True): Generator('json', 'latex', True),
    ('json', 'pdf', False): Generator('json', 'pdf', False),
    ('json', 'pdf', True): Generator('json', 'pdf', True),
    ('json', 'docx', False): Generator('json', 'docx', False),
    ('json', 'docx', True): Generator('json', 'docx', True),
    ('json', 'html', False): Generator('json', 'html', False),
    ('json', 'html', True): Generator('json', 'html', True),
    ('json', 'epub', False): Generator('json', 'epub', False),
    ('json', 'epub', True): Generator('json', 'epub', True),
    ('epub', 'mobi', False): MobiGenerator(),
    ('epub', 'mobi', True): MobiGenerator(),
    'metadata': MetadataGenerator(),
    'concatenate': ConcatenateGenerator(),
    'plugin': PluginGenerator(),
}).items()))

def runner(preconditions, postcondition, generator,
           output_filename, *args):
    for precondition in preconditions:
        precondition.wait()
    try:
        result = generator.compile(*args)
        postcondition.set()
        return output_filename, True, result, None
    except subprocess.CalledProcessError as e:
        return output_filename, False, False, (
            'CalledProcessError: Command %s' % (
                ' '.join([
                    "'%s'" % x if re.search(r'[^A-Za-z0-9_.=/-]', x) else x
                    for x in e.cmd])))
    except:
        return output_filename, False, False, ''.join(
            traceback.format_exception(*sys.exc_info()))

class PluginStage:
    def schedule(self, cx, _):
        input_paths = set()
        for generator in generators.values():
            if generator.active(cx):
                for plugin_src in generator.plugins(cx):
                    input_paths.add(plugin_src)

        futures = []
        for input_path in input_paths:
            output_path = input_path

            preconditions = []
            assert output_path not in cx.event_graph
            postcondition = cx.manager.Event()
            cx.event_graph[output_path] = postcondition
            futures.append(
                cx.thread_pool.apply_async(
                    runner,
                    (preconditions, postcondition, generators['plugin'],
                     output_path, cx.client_cx, input_path, output_path)))
        return futures

class GeneratorStage:
    def __init__(self, input_dir, input_format, output_format, full):
        self.input_dir = input_dir
        self.input_format = intermediate_formats[input_format]
        self.output_format = formats[output_format]
        self.full = full
        self.generator = generators[(input_format, output_format, full)]

    def schedule(self, cx, input_filenames):
        if not self.generator.active(cx):
            return []

        input_filenames = [
            filename for filename in input_filenames
            if filename in cx.output_filenames[self.input_format.name]]

        if self.full:
            input_filenames = ['all.json']
        input_dir = cx.input_dir if self.input_dir else cx.output_dir
        output_dir = cx.output_dir

        input_files = [
            cx.output_filenames[self.input_format.name][filename]
            for filename in input_filenames]
        if self.output_format.name not in cx.output_filenames:
            cx.output_filenames[self.output_format.name] = {}
        output_files = choose_output_filenames(
            input_filenames,
            self.output_format.file_ext,
            cx.output_filenames[self.output_format.name])

        input_paths = [
            os.path.join(input_dir, filename) for filename in input_files]
        output_paths = [
            os.path.join(output_dir, filename) for filename in output_files]

        plugins = self.generator.plugins(cx)

        futures = []
        for input_path, output_path in zip(input_paths, output_paths):
            preconditions = (
                [cx.event_graph[plugin] for plugin in plugins] +
                [cx.event_graph[input_path]])
            assert output_path not in cx.event_graph
            postcondition = cx.manager.Event()
            cx.event_graph[output_path] = postcondition
            futures.append(
                cx.thread_pool.apply_async(
                    runner,
                    (preconditions, postcondition, self.generator,
                     output_path, cx.client_cx, input_path, output_path)))
        return futures

class MetadataStage:
    def __init__(self, output_format):
        self.output_format = formats[output_format]
        self.generator = generators['metadata']

    def schedule(self, cx, input_filenames):
        output_dir = cx.output_dir
        if self.output_format.name not in cx.output_filenames:
            cx.output_filenames[self.output_format.name] = {}
        output_file = choose_output_filenames(
            ['metadata%s' % self.output_format.file_ext],
            self.output_format.file_ext,
            cx.output_filenames[self.output_format.name])[0]
        output_path = os.path.join(cx.output_dir, output_file)

        plugins = self.generator.plugins(cx)

        preconditions = [cx.event_graph[plugin] for plugin in plugins]
        assert output_path not in cx.event_graph
        postcondition = cx.manager.Event()
        cx.event_graph[output_path] = postcondition
        return [
            cx.thread_pool.apply_async(
                runner,
                (preconditions, postcondition, self.generator,
                 output_path, cx.client_cx, None, output_path))]

class ConcatenateStage:
    def __init__(self, input_format, output_format):
        self.input_format = intermediate_formats[input_format]
        self.output_format = formats[output_format]
        self.generator = generators['concatenate']

    def schedule(self, cx, input_filenames):
        input_filenames = [
            'metadata%s' % self.input_format.file_ext
        ] + sorted(input_filenames)

        input_dir = cx.output_dir
        output_dir = cx.output_dir

        input_files = [
            cx.output_filenames[self.input_format.name][filename]
            for filename in input_filenames]
        if self.output_format.name not in cx.output_filenames:
            cx.output_filenames[self.output_format.name] = {}
        output_file = choose_output_filenames(
            ['all%s' % self.output_format.file_ext],
            self.output_format.file_ext,
            cx.output_filenames[self.output_format.name])[0]

        input_paths = [
            os.path.join(input_dir, filename) for filename in input_files]
        output_path = os.path.join(output_dir, output_file)

        plugins = self.generator.plugins(cx)

        preconditions = (
            [cx.event_graph[plugin] for plugin in plugins] +
            [cx.event_graph[input_path] for input_path in input_paths])
        assert output_path not in cx.event_graph
        postcondition = cx.manager.Event()
        cx.event_graph[output_path] = postcondition
        return [
            cx.thread_pool.apply_async(
                runner,
                (preconditions, postcondition, self.generator,
                 output_path, cx.client_cx, input_paths, output_path))]

def configure_stages(cx):
    stages = [
        PluginStage(),
    ] + [
        GeneratorStage(
            input_dir=True,
            input_format=name,
            output_format='json',
            full=False)
        for name in input_formats.keys()
    ] + ([
        MetadataStage(
            output_format='json'),
        ConcatenateStage(
            input_format='json',
            output_format='json'),
        GeneratorStage(
            input_dir=False,
            input_format='json',
            output_format='latex',
            full=True),
        GeneratorStage(
            input_dir=False,
            input_format='json',
            output_format='pdf',
            full=True),
        GeneratorStage(
            input_dir=False,
            input_format='json',
            output_format='docx',
            full=True),
        GeneratorStage(
            input_dir=False,
            input_format='json',
            output_format='html',
            full=True),
        GeneratorStage(
            input_dir=False,
            input_format='json',
            output_format='epub',
            full=True),
        GeneratorStage(
            input_dir=False,
            input_format='epub',
            output_format='mobi',
            full=True),
    ] if cx.full else []) + ([
        GeneratorStage(
            input_dir=False,
            input_format='json',
            output_format='latex',
            full=False),
        GeneratorStage(
            input_dir=False,
            input_format='json',
            output_format='pdf',
            full=False),
        GeneratorStage(
            input_dir=False,
            input_format='json',
            output_format='docx',
            full=False),
        GeneratorStage(
            input_dir=False,
            input_format='json',
            output_format='html',
            full=False),
        GeneratorStage(
            input_dir=False,
            input_format='json',
            output_format='epub',
            full=False),
        GeneratorStage(
            input_dir=False,
            input_format='epub',
            output_format='mobi',
            full=False),
    ] if cx.part else [])
    return stages

###
### Driver
###

def build(cx):
    stages = configure_stages(cx)

    if cx.project_vcs is None:
        assert Exception('Cannot determine VCS')

    input_filenames = []
    for input_format in input_formats.values():
        if input_format.active(cx):
            filenames = find_input_filenames(
                cx.input_dir, input_format.file_ext)
            input_filenames.extend(filenames)
            cx.output_filenames[input_format.name] = dict(
                zip(filenames, filenames))

    if not os.path.exists(cx.output_dir):
        os.mkdir(cx.output_dir)
    for filename in input_filenames:
        dirname = os.path.join(cx.output_dir, os.path.dirname(filename))
        if not os.path.exists(dirname):
            os.mkdir(dirname)

    trivial_event = cx.manager.Event()
    trivial_event.set()
    for filename in input_filenames:
        input_path = os.path.join(cx.input_dir, filename)
        cx.event_graph[input_path] = trivial_event

    futures = []
    for stage in stages:
        futures.extend(stage.schedule(cx, input_filenames))

    for future in futures:
        filename, status, result, error = future.get()
        if not status:
            raise Exception('Failed to create %s:\n\n%s' % (filename, error))
        if result:
            print('Created %s' % filename)

def update(cx):
    if cx.root_vcs != 'git':
        raise Exception('Unable to update BookMD')
    vcs_update('git', cx.root_dir)

commands = {
    'build': build,
    'update': update,
}

def driver():
    parser = argparse.ArgumentParser(
        description='BookMD is a simple book authoring tool.')
    parser.add_argument('command', nargs='?', choices=commands.keys(),
                        default='build')
    parser.add_argument('-C', '--directory', required=False,
                        default=os.getcwd(), dest='project_dir',
                        help='Location of the project directory to build')
    parser.add_argument('-c', '--config', required=False,
                        default='_bookmd.json', dest='config_file',
                        help='Location of the project configuration to build')
    parser.add_argument('--stack', action='store_true',
                        default=True, dest='stack',
                        help='Use Stack for managing Pandoc installation (note: exclusive with --with-pandoc)')
    parser.add_argument('--no-stack', action='store_false',
                        default=True, dest='stack',
                        help='Do NOT use Stack for managing Pandoc installation')
    parser.add_argument('--with-stack', required=False,
                        help='Location of Stack binary (note: requires --stack)')
    parser.add_argument('--with-pandoc', required=False,
                        help='Location of Pandoc binary (note: exclusive with --stack)')
    parser.add_argument('-p',  '--only-part', action='store_true',
                        dest='only_part',
                        help='Build only part (not full) files')
    parser.add_argument('-f',  '--only-full', action='store_true',
                        dest='only_full',
                        help='Build only full (not part) files')
    parser.add_argument('-j', nargs='?', type=int,
                        dest='thread_count',
                        help='Number of threads to use for building')
    args = parser.parse_args()

    if args.only_part and args.only_full:
        raise Exception(
            'Options --only-part and --only-full are mutually exclusive')
    part = args.only_part or not args.only_full
    full = args.only_full or not args.only_part

    if args.with_stack is not None and not args.stack:
        raise Exception(
            'Option --with-stack requires --stack')

    stack_exe = args.with_stack or ('stack' if args.stack else None)

    if args.stack and args.with_pandoc is not None:
        raise Exception(
            'Options --stack and --with-pandoc are mutually exclusive')

    pandoc_exe = args.with_pandoc or (None if args.stack else 'pandoc')

    project_dir = os.path.realpath(args.project_dir)
    root_dir = os.path.dirname(os.path.realpath(__file__))
    config_path = os.path.join(project_dir, args.config_file)

    # Update is special; needs to happen before initializing the
    # context because we might not be inside a valid BookMD project.
    skip_config = args.command == 'update'
    cx = ServerContext(project_dir, root_dir, config_path, skip_config,
                       part, full, args.stack, stack_exe, pandoc_exe, args.thread_count)
    commands[args.command](cx)

if __name__ == '__main__':
    driver()
