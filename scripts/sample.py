#!/usr/bin/env python3
#### Copyright (c) 2013-2021, Elliott Slaughter <elliottslaughter@gmail.com>
####
#### Permission is hereby granted, free of charge, to any person
#### obtaining a copy of this software and associated documentation
#### files (the "Software"), to deal in the Software without
#### restriction, including without limitation the rights to use, copy,
#### modify, merge, publish, distribute, sublicense, and/or sell copies
#### of the Software, and to permit persons to whom the Software is
#### furnished to do so, subject to the following conditions:
####
#### The above copyright notice and this permission notice shall be
#### included in all copies or substantial portions of the Software.
####
#### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#### NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
#### HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
#### WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#### OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#### DEALINGS IN THE SOFTWARE.
####

# Requires Python >= 3.5.

import argparse
import json
import random
import subprocess

def parse(filename):
    return json.loads(subprocess.check_output(['pandoc', '-t', 'json', filename]))

def unparse(value):
    subprocess.run(['pandoc', '-f', 'json', '-t', 'markdown', '--atx-headers'],
                   input=json.dumps(value),
                   encoding='utf-8',
                   check=True)

def partition(blocks):
    result = []
    section = []
    for block in blocks:
        if block['t'] == 'Header' and block['c'][0] == 1:
            if len(section) > 0:
                result.append(section)
                section = []
        section.append(block)
    if len(section) > 0:
        result.append(section)
    return result

def driver(filename):
    doc = parse(args.filename)
    doc['blocks'] = random.choice(partition(doc['blocks']))
    unparse(doc)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='BookMD is a simple book authoring tool.')
    parser.add_argument('filename')
    args = parser.parse_args()

    driver(args.filename)
