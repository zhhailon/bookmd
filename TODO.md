# Tests

  * docx output with reference template
  * epub output with cover, metadata, style templates

# Cleanup

  * Decide how long to maintain _bookmd.json compatibility with Pandoc 1.x for:
      * --latex-engine (now --pdf-engine)
      * --no-tex-ligatures (now -smart extension)
