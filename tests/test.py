#!/usr/bin/env python3
#### Copyright (c) 2013-2021, Elliott Slaughter <elliottslaughter@gmail.com>
####
#### Permission is hereby granted, free of charge, to any person
#### obtaining a copy of this software and associated documentation
#### files (the "Software"), to deal in the Software without
#### restriction, including without limitation the rights to use, copy,
#### modify, merge, publish, distribute, sublicense, and/or sell copies
#### of the Software, and to permit persons to whom the Software is
#### furnished to do so, subject to the following conditions:
####
#### The above copyright notice and this permission notice shall be
#### included in all copies or substantial portions of the Software.
####
#### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#### NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
#### HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
#### WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#### OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#### DEALINGS IN THE SOFTWARE.
####

from __future__ import print_function, unicode_literals

import argparse, json, os, shutil, subprocess, sys, traceback

def test(bookmd_dir, test_dir, python, stack):
    with open(os.path.join(test_dir, '_bookmd.json')) as f:
        config = json.load(f)
    output_dir = os.path.join(test_dir, config['Output Directory'])
    bookmd = os.path.join(bookmd_dir, 'bookmd.py')
    check = os.path.join(test_dir, 'check.py')
    stack_flag = ['--stack'] if stack else ['--no-stack']

    print('Testing %s' % test_dir)
    try:
        shutil.rmtree(output_dir)
    except OSError:
        pass # Ok if directory doesn't exist
    subprocess.check_call([python, bookmd] + stack_flag, cwd = test_dir)
    subprocess.check_call([python, check], cwd = test_dir)

red = "\033[1;31m"
green = "\033[1;32m"
clear = "\033[0m"

def driver(python, stack, verbose):
    print('Using %s' % python)
    root_dir = os.path.dirname(os.path.realpath(__file__))
    bookmd_dir = os.path.dirname(root_dir)
    passed, failed = 0, 0
    for test_dir in os.listdir(root_dir):
        test_path = os.path.join(root_dir, test_dir)
        if os.path.isdir(test_path):
            try:
                test(bookmd_dir, test_path, python, stack)
            except:
                if verbose:
                    traceback.print_exc()
                print('[%sFAIL%s] %s' % (red, clear, test_path))
                failed += 1
            else:
                print('[%sPASS%s] %s' % (green, clear, test_path))
                passed += 1

    print('Passed %2d of %2d tests (%5.1f%%)' % (
        passed, passed + failed,
        (float(100*passed)/(passed + failed))))
    if failed > 0:
        raise Exception('Some tests failed')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--python', default=sys.executable, dest='python')
    parser.add_argument('--stack', action='store_true',
                        default=True, dest='stack')
    parser.add_argument('--no-stack', action='store_false',
                        default=True, dest='stack')
    parser.add_argument('-v', '--verbose', action='store_true', dest='verbose')
    args = parser.parse_args()

    driver(args.python, args.stack, args.verbose)
