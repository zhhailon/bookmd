#!/usr/bin/env python3
#### Copyright (c) 2013-2021, Elliott Slaughter <elliottslaughter@gmail.com>
####
#### Permission is hereby granted, free of charge, to any person
#### obtaining a copy of this software and associated documentation
#### files (the "Software"), to deal in the Software without
#### restriction, including without limitation the rights to use, copy,
#### modify, merge, publish, distribute, sublicense, and/or sell copies
#### of the Software, and to permit persons to whom the Software is
#### furnished to do so, subject to the following conditions:
####
#### The above copyright notice and this permission notice shall be
#### included in all copies or substantial portions of the Software.
####
#### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#### NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
#### HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
#### WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#### OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#### DEALINGS IN THE SOFTWARE.
####

string_docx_00 = 'This is the first chapter in docx.'
string_docx_02 = 'This is the third chapter in docx.'
string_md_00 = 'This is the first chapter in Markdown.'
string_md_01 = 'This is the second chapter in Markdown.'

def check():
    # Some more specific checks in the HTML files.
    with open('build/all.html') as f:
        c = f.read()

        index_docx_00 = c.find(string_docx_00)
        index_docx_02 = c.find(string_docx_02)
        index_md_00 = c.find(string_md_00)
        index_md_01 = c.find(string_md_01)

        assert index_docx_00 >= 0
        assert index_docx_02 >= 0
        assert index_md_00 >= 0
        assert index_md_01 >= 0

        # The conflict files 00.docx and 00.md might be in either
        # order, but both should be before the following
        # chapters. Also, 01.md should be before 02.docx.
        assert index_docx_00 < index_docx_02
        assert index_docx_00 < index_md_01
        assert index_md_00 < index_docx_02
        assert index_md_00 < index_md_01
        assert index_md_01 < index_docx_02

if __name__ == '__main__':
    check()
