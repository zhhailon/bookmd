BookMD is a simple book authoring tool for converting Markdown to PDF,
HTML, ePub, and Amazon KF8.

BookMD was initially created to serve the author's personal need for
such a tool. BookMD is intended to be simple and flexible, integrates
well with traditional version control systems, and produces output in
a variety of formats. The author specifically wanted to avoid formats
like Microsoft's `.doc` and `.docx` and Open/LibreOffice's `.odt`,
which tend to lose semantic information about the text, and DocBook,
which, being an XML format, tends to be ugly and tedious to read and
write.

BookMD was inspired by [Jekyll](http://jekyllrb.com/), a simple,
blog-aware, static website generator. BookMD follows Jekyll's design
principles, although the implementation differs substantially. For
example, BookMD uses [Pandoc](http://johnmacfarlane.net/pandoc/) to
parse and generate output files in a variety of formats. BookMD takes
care to preserve compatibility with basic Jekyll usage, so BookMD can
generate output for unmodified Jekyll source directories. However, as
a result of the implementation differences between BookMD and Jekyll,
BookMD does not (and is unlikely to ever) support the advanced
customization options that Jekyll provides.

Note that while BookMD has been used for the author's personal
projects for some time, it should be considered experimental software
and therefore unstable and subject to change at any time.

# Books Published with BookMD

The following is a list of books that have been published using BookMD.

  * [*Language As Disclosure*](https://www.amazon.com/dp/B019QSJ78W)
    by Carolyn Norman Slaughter
  * [*Time "Matters"*](https://www.amazon.com/dp/B083RKCFMC) by
    Carolyn Norman Slaughter

# Demo Project

This is a version of *Sense and Sensibility* by Jane Austen, rendered
to PDF, ePub, and Mobi using BookMD.

  * [PDF](https://bitbucket.org/elliottslaughter/bookmd-sample/downloads/latest.pdf) (formatted for 5.5x8.5-inch paper)
  * [ePub](https://bitbucket.org/elliottslaughter/bookmd-sample/downloads/latest.epub)
  * [Mobi](https://bitbucket.org/elliottslaughter/bookmd-sample/downloads/latest.mobi)
  * [Project Source](https://bitbucket.org/elliottslaughter/bookmd-sample)

# Installing

Download the repository and either symlink `bookmd.py` into your
personal `bin` directory, or add the entire directory to `PATH`. In
addition, you will need to install the following dependencies:

  * Python version >= 2.7 or >= 3.3: <http://python.org/>
  * Stack: <https://docs.haskellstack.org/en/stable/README/>
      * Note: If you prefer not to use Stack, you can use a system
        installation of GHC and Pandoc instead. See [non-Stack
        Installation](#markdown-header-non-stack-installation), below.

After installing dependences, run the following command:

```
cd bookmd && stack install pandoc # note this will install the `pandoc` binary to ~/.local/bin
```

The following dependencies are optional and are only required to
generate output files of the specified type:

  * LaTeX (for PDF output):
      * <http://www.tug.org/texlive/> (for Linux)
      * <http://www.tug.org/protext/> (for Windows)
      * <http://www.tug.org/mactex/> (for macOS)
  * KindleGen (for MOBI output):
    <http://www.amazon.com/gp/feature.html?docId=1000234621>

For all packages where version requirements are not explicitly listed,
the newest version of each package is recommended.

# Using

Create a new Git or Mercurial repository with one subdirectory, named
`_posts`. For each chapter in the book, place a Markdown file in the
`_posts` directory containing the contents of that chapter. Note that
chapters are assumed to be sorted in lexicographic order. Run
`bookmd.py` from the root of the repository.

The first time you run BookMD for a given project, BookMD will ask you
for various metadata about the project. This metadata will be stored
in a file called `_bookmd.json` at the root of your repository. It is
generally recommended to version this file so that the history of the
file is recorded if changes to the metadata are made over time.

BookMD will then create a `_book` subdirectory containing the output
files for each chapter in a variety of formats, in addition to files
named `all.pdf`, `all.epub`, etc. containing all chapters concatenated
together. It is generally recommended that the `_book` directory
**NOT** be versioned, as there should be no need, given that all the
output files can be automatically regenerated if lost or damaged.

The final state of the repository should generally look like this:

    _book/
        all.epub
        all.html
        all.mobi
        all.pdf
        chapter_01.epub
        chapter_01.html
        chapter_01.mobi
        chapter_01.pdf
        ...

    _bookmd.json

    _posts/
        chapter_01.md
        ...

# Non-Stack Installation

Stack is recommended as it vastly simplifies the management of correct
GHC and Pandoc versions. However, if you prefer not to use it, you can
instead use a system installation of GHC (e.g., though a package
manager). Note that in this case, *you must still install Pandoc
through* `cabal-install`, since BookMD plugins may depend on having a
full Pandoc installation available (which is typically not included in
the system's package manager, assuming the package manager's copy of
Pandoc is even up to date).

  * GHC: <http://www.haskell.org/platform/>
  * Pandoc >= 2: <http://johnmacfarlane.net/pandoc/>
      * See `pandoc-1` branch for Pandoc 1.x support
      * Note: when installing Pandoc via Cabal version 2 or greater,
        please use `cabal v1-install pandoc`, otherwise Pandoc is not
        made available to GHC by default

# License

Copyright (c) 2013-2021, Elliott Slaughter <elliottslaughter@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
