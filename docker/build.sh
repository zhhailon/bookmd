#!/bin/bash

set -e

root_dir="$(dirname "${BASH_SOURCE[0]}")"
cd "$root_dir"

for version in 2.9.1.1; do
    docker build --build-arg PANDOC_VERSION=$version -t elliottslaughter/bookmd-test:pandoc-$version -f Dockerfile.pandoc-2 .
    docker push elliottslaughter/bookmd-test:pandoc-$version
done
